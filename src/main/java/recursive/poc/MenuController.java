package recursive.poc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MenuController {

    @Autowired
    MenuTableRepository repository;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping("/menu/{id}")
    public MenuTable retrieveMenu(@PathVariable("id") Long id) {
        MenuTable locatedMenu =  repository.getOne(id);
        return locatedMenu;
    }
}
