package recursive.poc;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "menu")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class MenuTable {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "parent_id", referencedColumnName = "id")
    @NotFound(action= NotFoundAction.IGNORE)
    @JsonBackReference
    private MenuTable parent;

    @OneToMany(mappedBy = "parent", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<MenuTable> children = new HashSet<MenuTable>();

    public MenuTable() {
    }

    public MenuTable(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MenuTable getParent() {
        return parent;
    }

    public void setParent(MenuTable parent) {
        this.parent = parent;
    }

    public Set<MenuTable> getChildren() {
        return children;
    }

    public void setChildren(Set<MenuTable> children) {
        this.children = children;
    }
}
